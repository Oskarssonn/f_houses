fx_version 'cerulean'
game 'gta5'
server_scripts {
    '@mysql-async/lib/MySQL.lua',
    'src/server.lua'
}
client_scripts {
    'src/client.lua'
}
shared_scripts {
    'src/utils.lua'
}