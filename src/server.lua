local ESX

local validationData = {}
local data, inventories, garages, keys = nil, {}, {}, {}

function validateData(data)
    local callback = 0
    for k, v in pairs(data) do
        if v.owned and v.identifier then
            callback = v.id > callback and v.id or callback
            validationData[v.id ~= nil and v.id or callback] = v.identifier
            data[k].id = data[k].id ~= nil and data[k].id or callback
            data[k].identifier = nil
        end
    end
    Utils.Info("House data validated")
end

function addHouseToDatabase(house)
    MySQL.Async.execute('INSERT INTO houses (name, position, owned, price, tier, garage, garagePos, garageOut, heading, id, identifier, frontDoor, management, inventory, wardrobe) VALUES (@name, @position, @owned, @price, @tier, @garage, @garagePos, @garageOut, @heading, @id, @identifier, @frontDoor, @management, @inventory, @wardrobe)', {
        ['@name'] = house.name,
        ['@position'] = json.encode(house.position),
        ['@owned'] = house.owned,
        ['@price'] = house.price,
        ['@tier'] = house.tier,
        ['@garage'] = house.garage,
        ['@garagePos'] = json.encode(house.garagePos),
        ['@garageOut'] = json.encode(house.garageOut),
        ['@heading'] = house.heading or 0,
        ['@id'] = house.id,
        ['@identifier'] = house.identifier,
        ['@frontDoor'] = json.encode(house.frontDoor),
        ['@management'] = json.encode(house.management),
        ['@inventory'] = json.encode(house.inventory),
        ['@wardrobe'] = json.encode(house.wardrobe)
    })

    MySQL.Async.execute('INSERT INTO houses_inventory (id, weapons, items, dirty) VALUES (@id, @weapons, @items, @dirty)', {
        ['@id'] = house.id,
        ['@weapons'] = json.encode({}),
        ['@items'] = json.encode({}),
        ['@dirty'] = json.encode({})
    })
    if house.garage then
        MySQL.Async.execute('INSERT INTO houses_garage (id, vehicle) VALUES (@id, @vehicle)', {
            ['@id'] = house.id,
            ['@vehicle'] = json.encode({})
        })
    end
end

function getHouses()
    CreateThread(function()
        MySQL.Async.fetchAll('SELECT * FROM houses_inventory', {}, function(dbInv)
            for _, v in pairs(dbInv) do
                inventories[v.id] = v
            end
            Utils.Info("Inventories loaded, inventories = " .. json.encode(inventories))
        end)

        MySQL.Async.fetchAll('SELECT * FROM houses_garage', {}, function(dbCar)
            for _, v in pairs(dbCar) do
                garages[v.id] = v
            end
            Utils.Info("Garages loaded, garages = " .. json.encode(garages))
        end)

        MySQL.Async.fetchAll('SELECT * FROM houses_keys', {}, function(dbKey)
            for _, v in pairs(dbKey) do
                keys[v.id] = v
            end
            Utils.Info("Keys loaded, keys = " .. json.encode(keys))
        end)

        MySQL.Async.fetchAll('SELECT * FROM houses', {}, function(houses)
            data = houses
            Utils.Info("Houses loaded")
        end)
        repeat Wait(25) until data ~= nil
        validateData(data)
    end)
end

function registerCallbacks()
    ESX.RegisterServerCallback("f_houses:getData", function(source, cb)
        cb(data)
    end)

    ESX.RegisterServerCallback("f_houses:validateData", function(source, cb, id)
        if validationData[id] then
            cb(true)
        else
            cb(false)
        end
    end)

    ESX.RegisterServerCallback("f_houses:getOwned", function(source, cb)
        local identifier = ESX.GetPlayerFromId(source).getIdentifier()
        for _, v in pairs(data) do
            if v.owned and validationData[v.id] == identifier then
                cb(v)
                return
            end
        end
        cb(false)
    end)

    ESX.RegisterServerCallback("test:getIdent", function(source, cb)
        cb(ESX.GetPlayerFromId(source).getIdentifier())
    end)

    ESX.RegisterServerCallback("f_houses:giveKeys", function(source, cb, friend, house)
        local xTarget = ESX.GetPlayerFromId(friend)

        MySQL.Async.execute('INSERT INTO houses_keys (id, data) VALUES (@id, @data)', {
            ['@id'] = house.id,
            ['@data'] = json.encode({xTarget.getName(), xTarget.getIdentifier()}),
        })
        cb(true)
    end)

    ESX.RegisterServerCallback("f_houses:getKeys", function(source, cb, house)
        MySQL.Async.fetchAll('SELECT * FROM houses_keys WHERE id = @id', { ['@id'] = house.id }, function(keylist)
            if #keylist > 0 then
                cb(json.encode(keylist))
            else
                cb(false)
            end
        end)
    end)

    ESX.RegisterServerCallback("f_houses:removeKeys", function(source, cb, identifier, house)
        MySQL.Async.execute('DELETE FROM houses_keys WHERE id = @id AND identifier = @identifier', {
            ['@id'] = house.id,
            ['@identifier'] = identifier
        })
        cb(true)
    end)

    ESX.RegisterServerCallback("f_houses:checkAdmin", function(source, cb)
        local isAdmin = ESX.GetPlayerFromId(source).getGroup() ~= "user"
        cb(isAdmin)
    end)

    ESX.RegisterServerCallback("f_houses:createHouse", function(source, cb, house)
        local isAdmin = ESX.GetPlayerFromId(source).getGroup() ~= "user"
        if not isAdmin then
            cb(false, nil)
            return
        end
        house.owned = false
        house.id = #data + 1
        house.identifier = ""
        if house.price and house.position and house.tier and house.name and house.frontDoor and house.management and house.inventory and house.wardrobe then
            cb(true)
            addHouseToDatabase(house)
            TriggerClientEvent("f_houses:addHouse", -1, house)
        else
            cb(false)
        end
    end)

    Utils.Info("Registered callbacks")
end

RegisterServerEvent("f_houses:buyHouse", function(house)
    local xPlayer = ESX.GetPlayerFromId(source)
    local identifier = xPlayer.getIdentifier(source)

    if xPlayer.getMoney() >= house.price then
        xPlayer.removeAccountMoney('money', house.price)

        MySQL.Async.execute('UPDATE houses SET owned = @owned, identifier = @identifier WHERE id = @id', {
            ['@id'] = house.id,
            ['@owned'] = 1,
            ['@identifier'] = identifier,
        })

        data[house.id].owned = 1
        data[house.id].identifier = identifier
        validateData(data)
        
        xPlayer.showNotification("House bought for " ..house.price.. "$")
        TriggerClientEvent('f_houses:syncHouses', -1)
        TriggerClientEvent('f_houses:syncBlips', -1, house.id, false, 'buy')
        TriggerClientEvent('f_houses:syncBlips', source, house.id, true, 'buy')
    else
        xPlayer.showNotification("You don't have enough cash.")
    end
end)

RegisterServerEvent("f_houses:sellHouse", function(house)
    local xPlayer = ESX.GetPlayerFromId(source)

    MySQL.Async.execute('UPDATE houses SET owned = @owned, identifier = @identifier WHERE id = @id', {
        ['@id'] = house.id,
        ['@owned'] = 0,
        ['@identifier'] = '',
    })

    TriggerClientEvent('f_houses:syncBlips', -1, house.id, nil, 'sell')
    xPlayer.giveAccountMoney('money', house.price)
    xPlayer.showNotification("House sold for " ..house.price.. "$")
end)

function Init()
    ESX = exports["es_extended"]:getSharedObject()
    if not ESX then Utils.Info("Failed to init ESX") return end
    Utils.Info("ESX initialized")
    getHouses()
    registerCallbacks()
end

Init()
