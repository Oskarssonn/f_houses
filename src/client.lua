local ESX
local houses, ownedHouse, currentHouse, currentObjs, leavePos = nil, nil, nil, nil, nil
local blips = {}
local insideHouse, creating = false, false

function putCarInGarage(house)
    local ped = PlayerPedId()
    local vehicle = GetVehiclePedIsIn(ped, false)
    local vehicleProperties = ESX.Game.GetVehicleProperties(vehicle)
    ESX.TriggerServerCallback('f_houses:addToGarage', function(result)
        if result then
            TaskLeaveVehicle(ped, vehicle, 0)
        repeat Wait(10) until not IsPedInVehicle(ped, vehicle, false)
        Wait(850)
        ESX.Game.DeleteVehicle(vehicle)
            ESX.ShowNotification("You put your car ~b~" .. vehicleProperties.plate .. "~w~ in the garage.")
        else
            ESX.ShowNotification("You ~r~can't~w~ put this car in the garage for some reason")
        end
    end, house.id, vehicleProperties)
end

function enterGarage(house)
    local title, elements = "Garage", {}
    ESX.TriggerServerCallback("f_houses:getCars", function(cars)
        for i = 1, #cars do
            table.insert(elements, {label = cars[i].plate, value = cars[i].plate})
        end
    end, house.id)
end

function openInventory(house)
    local title, elements = house.name, {}
    table.insert(elements, {label = "Take weapons", value = "t_weap"})
    table.insert(elements, {label = "Put weapons", value = "p_weap"})
    table.insert(elements, {label = "Take items", value = "t_item"})
    table.insert(elements, {label = "Put items", value = "p_item"})
    table.insert(elements, {label = "Take money", value = "t_money"})
    table.insert(elements, {label = "Put money", value = "p_money"})
    ESX.UI.Menu.CloseAll()
    ESX.UI.Menu.Open("default", GetCurrentResourceName(), "house_inventory", {
        title = title,
        align = "top-left",
        elements = elements
    }, function(data, menu)
        menu.close()
    end, function(_d, menu)
        menu.close()
    end)
end

function openManagement(house)
    local title, elements = house.name, {}
    table.insert(elements, {label = "Give Keys", value = "give_keys"})
    table.insert(elements, {label = "Remove Keys", value = "remove_keys"})
    ESX.UI.Menu.CloseAll()
    ESX.UI.Menu.Open("default", GetCurrentResourceName(), "house_management", {
        title = title,
        align = "top-left",
        elements = elements
    }, function(data, menu)
        local action = data.current.value
        if action == "give_keys" then
            local friend, friendDist = ESX.Game.GetClosestPlayer()
            if friendDist < 3 then
                ESX.ShowNotification("Make sure your friend is standing right next to you!")
                return
            else
                ESX.TriggerServerCallback("f_houses:giveKeys", function(success)
                    if success then
                        ESX.ShowNotification("You gave your friend the keys!")
                    else
                        ESX.ShowNotification("You don't have any keys!")
                    end
                end, friend, house)
            end
        elseif action == "remove_keys" then
            ESX.TriggerServerCallback("f_houses:getKeys", function(keylist)
                if keylist then
                    local elements = {}
                    keylist = json.decode(keylist)
                    for k, v in pairs(keylist) do
                        table.insert(elements, {label = v.name, value = v.identifier})
                    end
                    ESX.UI.Menu.CloseAll()
                    ESX.UI.Menu.Open("default", GetCurrentResourceName(), "house_removekey", {
                        title = 'Keylist',
                        align = "top-left",
                        elements = elements
                    }, function(data, menu)
                        ESX.TriggerServerCallback("f_houses:removeKeys", function(success)
                            if success then
                                ESX.ShowNotification("You removed keys from " ..data.current.label)
                            end
                        end, data.current.value, house)
                        menu.close()
                    end, function(_d, menu)
                        menu.close()
                    end)
                else
                    ESX.ShowNotification("You have not given any keys yet.")
                end
            end, house)
        end
    end, function(_, menu)
        menu.close()
    end)
end

function openWardrobe(house)
    local elements, title = {}, house.name
    table.insert(elements, {label = "Open Wardrobe", value = "open_wardrobe"})
    ESX.UI.Menu.CloseAll()
    ESX.UI.Menu.Open("default", GetCurrentResourceName(), "wardrobe_menu", {
        title = title,
        align = "top-left",
        elements = elements
    }, function(data, menu)
        local action = data.current.value
        
    end, function(_, menu) 
        menu.close()
    end)
end

function openHouseMenu(owner, house)
    local elements, title = {}, house.name
    if owner then
        table.insert(elements, {label = "Enter House", value = "enter_house"})
        table.insert(elements, {label = "Sell House - " .. house.price * 0.85 .. "€", value = "sell_house"})
        table.insert(elements, {label = "Transfer Ownership", value = "transfer_ownership"})
    elseif house.owned then
        if not ownedHouse then
            table.insert(elements, {label = "Leave Offer", value = "leave_offer"})
        end
        table.insert(elements, {label = "Knock", value = "knock"})
    elseif not house.owned and not ownedHouse then
        table.insert(elements, {label = "Buy House - " .. house.price .. "€", value = "buy_house"})
    end
    ESX.UI.Menu.CloseAll()
    ESX.UI.Menu.Open("default", GetCurrentResourceName(), "house_menu", {
        title = title,
        align = "top-left",
        elements = elements
    }, function(data, menu)
        local action = data.current.value
        if action == "enter_house" then
            local position = house.position
            if house.tier == 1 then
                leavePos = GetEntityCoords(PlayerPedId())
                local data = exports['mythic_interiors']:CreateHotelFurnished({x = ret.position.x, y = ret.position.y, z = -ret.position.z})
                insideHouse = true ; currentHouse = house ; currentObjs = data[1]
            elseif house.tier == 2 then
                leavePos = GetEntityCoords(PlayerPedId())
                local data = exports['mythic_interiors']:CreateTier1HouseFurnished({x = position.x, y = position.y, z = -position.z}, false)
                insideHouse = true ; currentHouse = house ; currentObjs = data[1]
            elseif house.tier == 3 then
                leavePos = GetEntityCoords(PlayerPedId())
                local data = exports['mythic_interiors']:CreateTier3House({x = position.x, y = position.y, z = -position.z}, false)
                insideHouse = true ; currentHouse = house ; currentObjs = data[1]
            end
        elseif action == "buy_house" then
            TriggerServerEvent('f_houses:buyHouse', house)
        elseif action == "sell_house" then
            TriggerServerEvent('f_houses:sellHouse', house)
        end
        menu.close()
    end, function(_, menu) 
        menu.close()
    end)
end

function addBlip(data)
    local blip = AddBlipForCoord(data.position)
    SetBlipSprite(blip, 357)
    SetBlipDisplay(blip, 4)
    SetBlipScale(blip, 0.7)
    SetBlipAsShortRange(blip, true)
    SetBlipColour(blip, 0)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString("Empty House")
    SetBlipCategory(blip, 1)
    EndTextCommandSetBlipName(blip)
    blips[data.id] = blip
end

function createBlips()
    for _, v in pairs(houses) do
        local blip = AddBlipForCoord(v.position)
        SetBlipSprite(blip, 357)
        SetBlipScale(blip, 0.7)
        SetBlipAsShortRange(blip, true)
        BeginTextCommandSetBlipName("STRING")
        SetBlipDisplay(blip, 4)
        if ownedHouse and v.id == ownedHouse.id then
            SetBlipColour(blip, 11)
            AddTextComponentString("Home")
        else
            SetBlipColour(blip, 0)
            if not v.owned then
                AddTextComponentString("Empty House")
            end
            SetBlipCategory(blip, 1)
        end
        EndTextCommandSetBlipName(blip)
        blips[v.id] = blip
    end
    Utils.Info("Blips loaded")
end

function getOwned()
    ESX.TriggerServerCallback("f_houses:getOwned", function(data)
        ownedHouse = data
    end)
end

function exitShell()
    DoScreenFadeOut(500)
    repeat Wait(250) until not IsScreenFadingOut()
    SetEntityCoords(PlayerPedId(), leavePos.x, leavePos.y, leavePos.z, 0, 0, 0, false)
    Wait(100)
    DoScreenFadeIn(500)
    for _, v in pairs(currentObjs) do
        if DoesEntityExist(v) then
            Utils.Info("Deleting entity: " .. tostring(v))
            DeleteEntity(v)
        end
    end
    currentObjs = nil ; currentHouse = nil ; insideHouse = false ; leavePos = nil
end

function mainThread()
    Utils.Info("Main thread started")
    CreateThread(function()
        repeat Wait(150) until houses ~= nil
        Utils.Info("Houses loaded, #houses = " .. #houses)
        getOwned()
        repeat Wait(150) until ownedHouse ~= nil
        Utils.Info("Owned house loaded")
        createBlips()
        local ped = PlayerPedId()
        while true do
            if not insideHouse and not creating then
                local coords = GetEntityCoords(ped)
                local closest = { 
                    dist = math.huge,
                    garage = math.huge,
                    house = nil
                }
                for k, v in pairs(houses) do
                    closest = closest.dist > #(coords - v.position) and { dist = #(coords - v.position), house = k } or closest
                    local cHouse = closest.house ~= nil and houses[closest.house] or nil
                    if ownedHouse and ownedHouse.id == cHouse.id and cHouse ~= nil and cHouse.garage then
                        local distToGarage = #(coords - cHouse.garagePos)
                        local distToOut = #(coords - cHouse.garageOut)
                        if distToGarage < closest.dist then
                            closest.dist = distToGarage
                        end
                        if IsPedInAnyVehicle(ped, false) == 1 and distToOut < closest.dist then
                            closest.dist = distToOut
                        end
                    end
                end
                if closest.dist > 3 then
                    local speed = GetEntitySpeed(ped)
                    Wait((closest.dist < 100 and 250 or closest.dist / (speed / 5 < 5 and 1 or speed / 5)) % 7500)
                else
                    local currentHouse = houses[closest.house]
                    local owner = nil
                    ESX.TriggerServerCallback("f_houses:validateData", function(valid)
                        owner = valid
                    end, currentHouse.id)
                    repeat Wait(25) until owner ~= nil
                    while true do
                        Wait(1)
                        local inVeh = IsPedInAnyVehicle(ped, false)
                        if owner and not inVeh and currentHouse.garage and #(coords - currentHouse.garagePos) < #(coords - currentHouse.position) then
                            Draw3DText(currentHouse.garagePos, "Press ~r~[E]~s~ to enter the garage")
                            if IsControlJustPressed(0, 38) then
                                enterGarage(currentHouse)
                            end
                        elseif owner and inVeh == 1 and currentHouse.garage and #(coords - currentHouse.garageOut) < 4 then
                            Draw3DText(currentHouse.garageOut, "Press ~r~[E]~s~ to put car in garage")
                            if IsControlJustPressed(0, 38) then
                                putCarInGarage(currentHouse)
                            end
                        elseif inVeh ~= 1 then
                            Draw3DText(currentHouse.position, "Press ~r~[E]~s~ to view house info")
                            if IsControlJustPressed(0, 38) then
                                openHouseMenu(owner, currentHouse)
                            end
                        end
                        coords = GetEntityCoords(ped)
                        if #(coords - currentHouse.position) > 3 and not currentHouse.garage then
                            break
                        elseif (#(coords - currentHouse.position) > 3) and (#(coords - currentHouse.garagePos) > 3) then
                            if inVeh == 1 and (#(coords - currentHouse.garageOut) > 4) then
                                break
                            elseif not inVeh then 
                                break
                            end
                        elseif insideHouse or creating then
                            break
                        end
                    end
                end
            elseif insideHouse == true and not creating then
                Wait(0)
                local coords = GetEntityCoords(ped)
                if #(coords - currentHouse.frontDoor) < 3 then
                    Draw3DText(vector3(currentHouse.frontDoor.x, currentHouse.frontDoor.y, currentHouse.frontDoor.z + 1), "Press ~r~[E]~s~ to leave the house")
                    if IsControlJustPressed(0, 38) then
                        exitShell()
                    end
                elseif #(coords - currentHouse.management) < 3 then
                    Draw3DText(currentHouse.management, "Press ~r~[E]~s~ to manage the house")
                    if IsControlJustPressed(0, 38) then
                        openManagement(currentHouse)
                    end
                elseif #(coords - currentHouse.inventory) < 3 then
                    Draw3DText(currentHouse.inventory, "Press ~r~[E]~s~ to open inventory")
                    if IsControlJustPressed(0, 38) then
                        openInventory(currentHouse)
                    end
                elseif #(coords - currentHouse.wardrobe) < 3 then
                    Draw3DText(currentHouse.wardrobe, "Press ~r~[E]~s~ to wardrobe")
                    if IsControlJustPressed(0, 38) then
                        openWardrobe(currentHouse)
                    end
                end
            else
                Wait(2500)
            end
        end
    end)
end

function stringToVector3(argstr)
    if argstr == "null" then return nil end
    local tab = json.decode(argstr)
    return vector3(tab.x, tab.y, tab.z)
end

function getHouses()
    CreateThread(function()
        Wait(2500)
        ESX.TriggerServerCallback("f_houses:getData", function(data)
            houses = data
            for k, v in pairs(houses) do
                houses[k].position = stringToVector3(v.position)
                houses[k].frontDoor = stringToVector3(v.frontDoor)
                houses[k].management = stringToVector3(v.management)
                houses[k].garagePos = stringToVector3(v.garagePos)
                houses[k].garageOut = stringToVector3(v.garageOut)
                houses[k].inventory = stringToVector3(v.inventory)
                houses[k].wardrobe = stringToVector3(v.wardrobe)
            end
        end)
    end)
end

function promptCoords()
    while not IsControlJustPressed(0, 38) do
        ESX.ShowHelpNotification("Press ~INPUT_CONTEXT~ to confirm the coordinates")
        Wait(0)
    end
    return GetEntityCoords(PlayerPedId())
end

function prompt()
    local ret = 0
    DisplayOnscreenKeyboard(false, "FMMC_KEY_TIP8", "", "", "", "", "", 7)
    while UpdateOnscreenKeyboard() == 0 do
        HideHudAndRadarThisFrame()
        DisableAllControlActions(0)
        Wait(0)
    end
    if GetOnscreenKeyboardResult() then
        ret = GetOnscreenKeyboardResult()
    end
    return ret
end

function registerHouseCommand()
    CreateThread(function()
        RegisterCommand("createhouse", function()
            if insideHouse then return end
            ESX.TriggerServerCallback("f_houses:checkAdmin", function(admin)
                if not admin then return end
                creating = true
                local ret = {}
                local coords = GetEntityCoords(PlayerPedId())
                local streethash, _d = GetStreetNameAtCoord(coords.x, coords.y, coords.z)
                ret.name = GetStreetNameFromHashKey(streethash) .. " " .. math.random(1, 987)
                ESX.ShowNotification("Enter the tier of the house you want to create [1-3]")
                ret.tier = math.abs(tonumber(prompt())) % 4
                if ret.tier == 0 then ret.tier = 1 end
                ESX.ShowNotification("Enter the price of the house")
                ret.price = tonumber(prompt())
                ESX.ShowNotification("Add garage [y/n]")
                local garage = prompt()
                if garage == "y" then
                    Wait(100)
                    ESX.ShowNotification("Set the garage position")
                    ret.garagePos = promptCoords()
                    Wait(100)
                    ESX.ShowNotification("Set the garage spawn position")
                    ret.heading = GetEntityHeading(PlayerPedId())
                    ret.garageOut = promptCoords()
                    Wait(100)
                    ret.garage = true
                else
                    ret.garagePos = nil
                    ret.garage = false
                    ret.garageOut = nil
                    ret.heading = nil
                end
                ESX.ShowNotification("Set the front door position")
                ret.position = promptCoords()
                leavePos = ret.position
                Wait(250)
                local data
                if ret.tier == 1 then
                    data = exports['mythic_interiors']:CreateHotelFurnished({x = ret.position.x, y = ret.position.y, z = -ret.position.z})
                elseif ret.tier == 2 then
                    data = exports['mythic_interiors']:CreateTier1HouseFurnished({x = ret.position.x, y = ret.position.y, z = -ret.position.z}, false)
                elseif ret.tier == 3 then
                    data = exports['mythic_interiors']:CreateTier3House({x = ret.position.x, y = ret.position.y, z = -ret.position.z}, false)
                end
                insideHouse = true ; currentObjs = data[1]
                Wait(100)
                ESX.ShowNotification("Set the management position")
                ret.management = promptCoords()
                Wait(100)
                ESX.ShowNotification("Set the inventory position")
                Wait(100)
                ret.inventory = promptCoords()
                Wait(100)
                ESX.ShowNotification("Set the wardrobe position")
                ret.wardrobe = promptCoords()
                Wait(100)
                ESX.ShowNotification("Set the exit position")
                ret.frontDoor = promptCoords()
                Wait(250)
                exitShell()
                ESX.TriggerServerCallback("f_houses:createHouse", function(valid)
                    if valid then
                        ESX.ShowNotification("House created")
                    else
                        ESX.ShowNotification("House creation failed")
                    end
                    creating = false
                end, ret)
            end)
        end, false)
    end)
    Utils.Info("Command registered: /createhouse")
end

RegisterNetEvent('f_houses:addHouse', function(data)
    houses[#houses + 1] = data
    addBlip(data)
end)

RegisterNetEvent('f_houses:syncHouses', function()
    getHouses()
end)

RegisterNetEvent('f_houses:syncBlips', function(id, owner, event)
    BeginTextCommandSetBlipName("STRING")
    if event == 'buy' and owner then
        SetBlipColour(blips[id], 11)
        SetBlipDisplay(blips[id], 4)
        AddTextComponentString("Home")
    elseif event == 'buy' and not owner then
        SetBlipDisplay(blips[id], 0)
    elseif event == 'sell' then
        SetBlipColour(blips[id], 0)
        SetBlipDisplay(blips[id], 4)
        AddTextComponentString("Empty House")
    end
    EndTextCommandSetBlipName(blips[id])
end)


function Draw3DText(coords, text)
    if not coords or not text then return end
	local onScreen, x, y = World3dToScreen2d(coords.x, coords.y, coords.z)
	SetTextScale(0.4, 0.4)
	SetTextOutline()
	SetTextDropShadow()
	SetTextDropshadow(2, 0, 0, 0, 255)
	SetTextFont(4)
	SetTextProportional(1)
	SetTextEntry('STRING')
	SetTextCentre(1)
	SetTextColour(255, 255, 255, 215)
	AddTextComponentString(text)
	DrawText(x, y)
    local factor = (string.len(text)) / 400
    DrawRect(x, y + 0.013, 0.015 + factor, 0.03, 0, 0, 0, 68)
end

function Init()
    ESX = exports["es_extended"]:getSharedObject()
    if not ESX then Utils.Info("Failed to init ESX") return end
    Utils.Info("ESX initialized")
    getHouses()
    registerHouseCommand()
    mainThread()
end

AddEventHandler('onClientResourceStart', function(eventName)
    if GetCurrentResourceName() == eventName then
        CreateThread(function()
            Wait(3000)
            Init()
        end)
    end
end)
