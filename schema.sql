CREATE TABLE IF NOT EXISTS houses (
    `name` varchar(255) NOT NULL,
    `position` varchar(255) NOT NULL,
    `owned` BOOLEAN NOT NULL,
    `price` int(50) NOT NULL,
    `tier` int(10) NOT NULL,
    `garage` BOOLEAN NOT NULL,
    `garagePos` varchar(255) NOT NULL,
    `garageOut` varchar(255) NOT NULL,
    `heading` int(10) NOT NULL,
    `id` int(10) NOT NULL,
    `identifier` varchar(255) NOT NULL,
    `frontDoor` varchar(255) NOT NULL,
    `management` varchar(255) NOT NULL,
    `inventory` varchar(255) NOT NULL,
    `wardrobe` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS houses_inventory (
    `id` int(10) NOT NULL,
    `weapons` varchar(255) NOT NULL,
    `items` varchar(255) NOT NULL,
    `dirty` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS houses_garage (
    `id` int(10) NOT NULL,
    `vehicle` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS houses_keys (
    `id` int(10) NOT NULL,
    `data` varchar(255) NOT NULL
);
